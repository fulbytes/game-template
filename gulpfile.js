var gulp = require('gulp'),
	express = require('express'), 
	browserSync = require('browser-sync');

var SERVER_PORT = 9000;
var server = express();
server.use(express.static('./app'));

gulp.task('serve', function(){
	server.listen(SERVER_PORT);
});

gulp.task('browser-sync', function(){
	//browserSync.init(['./app/**'], {
	browserSync({
		proxy: '192.168.59.103:5000',
		files: ['./app/**']
	});
});

// use default task to launch BrowserSync and watch JS files
gulp.task('default', ['browser-sync'], function () {
	// add browserSync.reload to the tasks array to make
	
	// all browsers reload after tasks are complete.
	//gulp.watch("./app/**", browserSync.reload);
});


gulp.task('build', [], function(){
	console.log('gulp build - started');
	console.log('build updated again');
});

